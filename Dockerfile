#Using golang image with alpine linux to reduce image size
FROM golang:1.13.3-alpine3.10 AS BASE

#Create and go the the required directory in docker
WORKDIR poc/sarama_kafka

#Copy toml and lock files to the current directory
COPY go.mod go.sum ./

# Get dependencies - will also be cached if we won't change mod/sum
RUN go mod download

#Copy other files in the host to the current directory
COPY . ./

#Perform go build
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o /kafka-producer .

#Use a new image to run the application
FROM scratch

#Copy the executable generated from BASE
COPY --from=BASE /kafka-producer ./

#Add resources files required for the executable to run
COPY config.toml ./

#Create entry point for your executable
ENTRYPOINT ["./kafka-producer"]