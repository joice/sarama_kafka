package main

import (
	"encoding/json"
	"fmt"
	"github.com/Shopify/sarama"
	"github.com/hashicorp/go-uuid"
	"sync"
	"time"
)

func main() {
	LoadConfig()

	producer := *GetKafkaProducer()

	defer func() {
		_ = producer.Close()
	}()
	//producerTopics := []string{"scaleio_alerts"}
	alertTypes := []string{"ALERT_LOW", "ALERT_MEDIUM", "ALERT_HIGH"}
	wg := sync.WaitGroup{}

	for i := 0; i < 10000; i++ {
		for j := 0; j < len(alertTypes); j++ {
			for _, topic := range ProducerTopics {
				wg.Add(1)
				msg := GenerateMessages(alertTypes[j])
				fmt.Println("alert message : ", msg)
				go KafkaMultiConnect(&producer, topic, &wg, msg)
				if j+1 == len(alertTypes) {
					j = -1
				}
				time.Sleep(50 * time.Second)
			}
		}
	}
	wg.Wait()
	fmt.Println("program exited")
}

func KafkaMultiConnect(producer *sarama.SyncProducer, topic string, wg *sync.WaitGroup, i string) {
	defer wg.Done()
	msg := &sarama.ProducerMessage{
		Topic:     topic,
		Value:     sarama.StringEncoder(i),
		Timestamp: time.Now().UTC(),
	}

	_, _, err := (*producer).SendMessage(msg)
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
}

func GenerateMessages(severity string) string {
	var alert Alert
	uid, _ := uuid.GenerateUUID()
	alert.AffectedObject = ao{
		Id:       uid,
		ObjectId: uid,
		Type:     uid,
	}
	alert.Name = uid
	alert.Id = uid
	alert.StartTime = time.Now().Format(time.RFC3339)
	alert.LastObserved = time.Now().Format(time.RFC3339)
	alert.Severity = severity
	alert.UUID = uid
	alert.Links = []link{
		{Rel: uid, Href: uid},
		{Rel: uid, Href: uid},
	}

	bytes, _ := json.Marshal(alert)
	return string(bytes)
}

type Alert struct {
	AffectedObject ao     `json:"affectedObject"`
	Severity       string `json:"severity"`
	AlertType      string `json:"alertType"`
	UUID           string `json:"uuid"`
	LastObserved   string `json:"lastObserved"`
	StartTime      string `json:"startTime"`
	Name           string `json:"name"`
	Id             string `json:"id"`
	Links          []link `json:"links"`
}

type ao struct {
	Type     string `json:"type"`
	Id       string `json:"id"`
	ObjectId string `json:"objectId"`
}

type link struct {
	Rel  string `json:"rel"`
	Href string `json:"href"`
}
