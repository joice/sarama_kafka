package main

import (
	"fmt"
	"github.com/Shopify/sarama"
	"github.com/spf13/viper"
	"log"
	"os"
	"strings"
	"sync"
)

const (
	kafkaBrokers  = "KAFKA_BROKERS"
	consumerTopic = "SNOW_CONSUMER_TOPIC"
	producerTopic = "SNOW_PRODUCER_TOPIC"
)

type Configuration struct {
	KafkaBrokers  string
	ConsumerTopic string
	ProducerTopic string
}

func NewProducer() *sarama.SyncProducer {
	//brokers := []string{"10.0.0.198:9094", "10.0.0.198:9093", "10.0.0.198:9092"} //strings.Split(GetEnv(BootstrapServers, bootstrapServerDefault), ",")
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = 5
	config.Producer.Return.Successes = true
	config.Version = sarama.V2_3_0_0

	// brokers = []string{"192.168.59.103:9092"}
	producer, err := sarama.NewSyncProducer(brokers, config)
	if err != nil {
		// Should not reach here
		fmt.Println("error occurred", err)
		panic(err)
	}

	return &producer
}

var (
	once           sync.Once
	writerInstance *sarama.SyncProducer
)

func GetKafkaProducer() *sarama.SyncProducer {
	// Singleton instance
	once.Do(func() {
		writerInstance = NewProducer()
	})
	return writerInstance
}

var config Configuration
var path = "."
var ProducerTopics []string
var brokers []string

func LoadConfig() {

	viper.SetConfigName("config")
	viper.AddConfigPath(path)
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalln("Config file not found:", err)
	}
	config.KafkaBrokers = viper.GetString("development.KafkaBrokers")
	config.ConsumerTopic = viper.GetString("development.ConsumerTopic")
	config.ProducerTopic = viper.GetString("development.ProducerTopic")

	ProducerTopics = strings.Split(GetEnv(producerTopic, config.ProducerTopic), ",")
	brokers = strings.Split(GetEnv(kafkaBrokers, config.KafkaBrokers), ",")
	fmt.Println(brokers)
}

func GetEnv(key, def string) (value string) {
	value = os.Getenv(key)
	if value == "" {
		value = def
	}
	return
}
